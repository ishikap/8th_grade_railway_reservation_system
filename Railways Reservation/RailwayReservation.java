import java.io.*;
import java.util.*;
class RailwayReservation
{
    public static void main(String args[])throws IOException
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("********************Railway Reservation********************");
        System.out.println("");
        System.out.println("Welcome to Railway Ticket Reservation Service");
        System.out.println("");
        //Initialising Variables
        int choice=0;
        char ch='0';
        int a=0;
        int b=0;
        int board=0;
        int destination=0;
        int bookdate=0;
        int trainno=0;
        String trainname="0";
        String timeofboarding="0";
        String timeofreaching="0";
        String from="0";
        String to="0";
        int date1=0;
        int date2=0;
        int coach=0;
        int i=0;
        String j="0";
        int gcol=0;
        int grow=0;
        int GenCompart [][] = new int [4][18];
        int GenCompart1 []= new int [73];
        int z=0;
        int seatno=0;
        int k=0;
        int randomnum=0;
        int reservationno=0;
        double fare=0.00;
        double totalfare=0.00;
        String name="0";
        String address="0";
        int age=0;
        char gender='0';

        label: while(a==0)
        {
            //Taking choice of booking or cancellation
            System.out.println("Please select option 1 or 2");
            System.out.println("1. Booking");
            System.out.println("2. Cancellation");
            choice=Integer.parseInt(br.readLine());
            //Checking if choice is valid or not
            if(choice==1||choice==2)
            {
                a=1;
            }
            else
            {
                while (b==0)
                {
                    System.out.println("Wrong Choice, Do you want to try again? y/n");
                    ch = (char) new InputStreamReader(System.in).read ();
                    if(ch=='Y'||ch=='y')
                    {
                        b=1;
                    }
                    else if(ch=='n'||ch=='N')
                    {
                        b=1;
                        break label;
                    }

                    else
                    {}
                }
            }
        }
        label: if (choice==1)
        {
            a=0;
            while(a==0)
            {
                //Menu for To-from
                System.out.println("Select from 1-5 as Station of board");
                System.out.println("1.  Mumbai");
                System.out.println("2.  Delhi");
                System.out.println("3.  Kolkata");
                System.out.println("4.  Dehradun");
                System.out.println("5.  Chennai");
                board=Integer.parseInt(br.readLine());
                System.out.println("Select from 1-5 as Destination");
                System.out.println("1.  Mumbai");
                System.out.println("2.  Delhi");
                System.out.println("3.  Kolkata");
                System.out.println("4.  Dehradun");
                System.out.println("5.  Chennai");
                destination=Integer.parseInt(br.readLine());
                //checking if to from  is valid
                if(board==destination)
                {
                    b=0;
                    while (b==0)
                    {
                        System.out.println("Station of Boarding and Destination are same. Do you want to try again? y/n");
                        ch = (char) new InputStreamReader(System.in).read ();
                        if(ch=='Y'||ch=='y')
                        {
                            b=1;
                        }
                        else if(ch=='n'||ch=='N')
                        {
                            b=1;
                            break label;
                        }

                        else
                        {}
                    }
                }
                else if(board>5||board<1||destination>5||destination<0)
                {
                    b=0;
                    while (b==0)
                    {
                        System.out.println("Wrong Choice, Do you want to try again? y/n");
                        ch = (char) new InputStreamReader(System.in).read ();
                        if(ch=='Y'||ch=='y')
                        {
                            b=1;
                        }
                        else if(ch=='n'||ch=='N')
                        {
                            b=1;
                            break label;
                        }

                        else
                        {}
                    }
                }
                else
                {
                    a=1;
                }
            }
            //Taking date
            a=0;
            while(a==0)
            {
                System.out.println("Enter any date in August 2012(from 1 to 30)");
                bookdate=Integer.parseInt(br.readLine());
                //checking if date is valid
                if(bookdate>30||bookdate<1)
                {
                    b=0;
                    while (b==0)
                    {
                        System.out.println("Wrong Date, Do you want to try again? y/n");
                        ch = (char) new InputStreamReader(System.in).read ();
                        if(ch=='Y'||ch=='y')
                        {
                            b=1;
                        }
                        else if(ch=='n'||ch=='N')
                        {
                            b=1;
                            break label;
                        }

                        else
                        {}
                    }
                }
                else
                {
                    a=1;
                }
            }
            //calculating arrival date
            date1=bookdate+1;
            date2=bookdate+2;
            if (bookdate==30)
            {
                date1=1;
                date2=2;
            }
            switch(board)
            {
                //train list if Station of board is 1
                case 1:
                switch(destination)
                {
                    case 2:
                    trainno=1001; 
                    trainname="Mumbai Delhi express";
                    if(bookdate==30)
                    {
                        timeofreaching="3.30pm    "+(date1)+" September 2012";
                    }
                    else
                    {
                        timeofreaching="3.30pm    "+(date1)+" August 2012";
                    }
                    timeofboarding="1.30pm    "+bookdate+" August 2012";                                    

                    System.out.println("Train Number: "+trainno);
                    System.out.println("Train Name: "+trainname);
                    System.out.println("Time of Boarding: "+timeofboarding);
                    System.out.println("Time of Reaching: "+timeofreaching);
                    from="Mumbai";
                    to="Delhi";
                    break;

                    case 3:
                    trainno=1002; 
                    trainname="Mumbai Kokata express";
                    if(bookdate==30)
                    {
                        timeofreaching="1.30pm    "+(date1)+" September 2012";
                    }
                    else
                    {
                        timeofreaching="1.30pm    "+(date1)+" August 2012";
                    }
                    timeofboarding="10.30am    "+bookdate+" August 2012";                                    

                    System.out.println("Train Number: "+trainno);
                    System.out.println("Train Name: "+trainname);
                    System.out.println("Time of Boarding: "+timeofboarding);
                    System.out.println("Time of Reaching: "+timeofreaching);
                    from="Mumbai";
                    to="Kolkata";
                    break;

                    case 4:
                    trainno=1003; 
                    trainname="Mumbai dehradun express";
                    if(bookdate==30)
                    {
                        timeofreaching="3.30am    "+(date1)+" September 2012";
                    }
                    else
                    {
                        timeofreaching="3.30am    "+(date1)+" August 2012";
                    }
                    timeofboarding="6.30am    "+bookdate+" August 2012";                                    

                    System.out.println("Train Number: "+trainno);
                    System.out.println("Train Name: "+trainname);
                    System.out.println("Time of Boarding: "+timeofboarding);
                    System.out.println("Time of Reaching: "+timeofreaching);
                    from="Mumbai";
                    to="Dehradun";
                    break;

                    case 5:
                    trainno=1004; 
                    trainname="Mumbai Chennai express";
                    if(bookdate==30)
                    {
                        timeofreaching="10.30am    "+(date2)+" September 2012";
                    }
                    else
                    {
                        timeofreaching="10.30am    "+(date2)+" August 2012";
                    }
                    timeofboarding="7.30am    "+bookdate+" August 2012";                                    

                    System.out.println("Train Number: "+trainno);
                    System.out.println("Train Name: "+trainname);
                    System.out.println("Time of Boarding: "+timeofboarding);
                    System.out.println("Time of Reaching: "+timeofreaching);
                    from="Mumbai";
                    to="Chennai";
                    break;

                }
                break;
                //train list if station of board is 2
                case 2:
                switch(destination)
                {
                    case 1:
                    trainno=1005; 
                    trainname="Delhi-Bombay Rajdhani";
                    if(bookdate==30)
                    {
                        timeofreaching="6.30am    "+(date1)+" September 2012";
                    }
                    else
                    {
                        timeofreaching="6.30am    "+(date1)+" August 2012";
                    }
                    timeofboarding="5.00am    "+bookdate+" August 2012";                                    

                    System.out.println("Train Number: "+trainno);
                    System.out.println("Train Name: "+trainname);
                    System.out.println("Time of Boarding: "+timeofboarding);
                    System.out.println("Time of Reaching: "+timeofreaching); 
                    from="Delhi";
                    to="Mumbai";
                    break;

                    case 3:
                    trainno=1006; 
                    trainname="Delhi-Kokata Rajdhani";
                    if(bookdate==30)
                    {
                        timeofreaching="12.30pm    "+(date1)+" September 2012";
                    }
                    else
                    {
                        timeofreaching="12.30pm    "+(date1)+" August 2012";
                    }
                    timeofboarding="11.00am    "+bookdate+" August 2012";                                    

                    System.out.println("Train Number: "+trainno);
                    System.out.println("Train Name: "+trainname);
                    System.out.println("Time of Boarding: "+timeofboarding);
                    System.out.println("Time of Reaching: "+timeofreaching);
                    from="Delhi";
                    to="Kolkata";
                    break;

                    case 4:
                    trainno=1007; 
                    trainname="Delhi-Dehradun Rajdhani";
                    timeofboarding="11.30am    "+bookdate+" August 2012";                                    
                    timeofreaching="3.30am    "+(bookdate)+" August 2012";
                    System.out.println("Train Number: "+trainno);
                    System.out.println("Train Name: "+trainname);
                    System.out.println("Time of Boarding: "+timeofboarding);
                    System.out.println("Time of Reaching: "+timeofreaching); 
                    from="Delhi";
                    to="Dehradun";
                    break;

                    case 5:
                    trainno=1008; 
                    trainname="Delhi-Chennai Rajdhani";
                    if(bookdate==30)
                    {
                        timeofreaching="1.30pm    "+(date2)+" September 2012";
                    }
                    else
                    {
                        timeofreaching="1.30pm    "+(date2)+" August 2012";
                    }
                    timeofboarding="9.30am    "+bookdate+" August 2012";                                    

                    System.out.println("Train Number: "+trainno);
                    System.out.println("Train Name: "+trainname);
                    System.out.println("Time of Boarding: "+timeofboarding);
                    System.out.println("Time of Reaching: "+timeofreaching); 
                    from="Delhi";
                    to="Chennai";
                    break;

                }
                break;

                //train list if station of board is 3
                case 3:
                switch(destination)
                {
                    case 1:
                    trainno=1009; 
                    trainname="Kolkata Mail";
                    if(bookdate==30)
                    {
                        timeofreaching="5.30am    "+(date1)+" September 2012";
                    }
                    else
                    {
                        timeofreaching="5.30am    "+(date1)+" August 2012";
                    }
                    timeofboarding="1.30am    "+bookdate+" August 2012";                                    

                    System.out.println("Train Number: "+trainno);
                    System.out.println("Train Name: "+trainname);
                    System.out.println("Time of Boarding: "+timeofboarding);
                    System.out.println("Time of Reaching: "+timeofreaching);
                    from="Kolkata";
                    to="Mumbai";
                    break;

                    case 2:
                    trainno=1010; 
                    trainname="Kolkata Delhi Fast Train";
                    if(bookdate==30)
                    {
                        timeofreaching="4.00am    "+(date1)+" September 2012";
                    }
                    else
                    {
                        timeofreaching="4.00am    "+(date1)+" August 2012";
                    }
                    timeofboarding="6.00am    "+bookdate+" August 2012";                                    

                    System.out.println("Train Number: "+trainno);
                    System.out.println("Train Name: "+trainname);
                    System.out.println("Time of Boarding: "+timeofboarding);
                    System.out.println("Time of Reaching: "+timeofreaching);
                    from="Kolkata";
                    to="Delhi";
                    break;

                    case 4:
                    trainno=1011; 
                    trainname="Kolkata dehradun mail";
                    if(bookdate==30)
                    {
                        timeofreaching="2.30pm    "+(date1)+" September 2012";
                    }
                    else
                    {
                        timeofreaching="2.30pm    "+(date1)+" August 2012";
                    }
                    timeofboarding="7.30am    "+bookdate+" August 2012";                                    

                    System.out.println("Train Number: "+trainno);
                    System.out.println("Train Name: "+trainname);
                    System.out.println("Time of Boarding: "+timeofboarding);
                    System.out.println("Time of Reaching: "+timeofreaching);
                    from="Kolkata";
                    to="Dehradun";
                    break;

                    case 5:
                    trainno=1012; 
                    trainname="Kolkata Chennai rail";
                    if(bookdate==30)
                    {
                        timeofreaching="9.30am    "+(date2)+" September 2012";
                    }
                    else
                    {
                        timeofreaching="9.30am    "+(date2)+" August 2012";
                    }
                    timeofboarding="10.30am    "+bookdate+" August 2012";                                    

                    System.out.println("Train Number: "+trainno);
                    System.out.println("Train Name: "+trainname);
                    System.out.println("Time of Boarding: "+timeofboarding);
                    System.out.println("Time of Reaching: "+timeofreaching); 
                    from="Kolkata";
                    to="Chennai";
                    break;

                }
                break;

                //train list if station of board is 4
                case 4:
                switch(destination)
                {
                    case 1:
                    trainno=1013; 
                    trainname="Dehradun Mail Express";
                    if(bookdate==30)
                    {
                        timeofreaching="12.30pm    "+(date1)+" September 2012";
                    }
                    else
                    {
                        timeofreaching="12.30pm    "+(date1)+" August 2012";
                    }
                    timeofboarding="5.30am    "+bookdate+" August 2012";                                    

                    System.out.println("Train Number: "+trainno);
                    System.out.println("Train Name: "+trainname);
                    System.out.println("Time of Boarding: "+timeofboarding);
                    System.out.println("Time of Reaching: "+timeofreaching); 
                    from="Dehradun";
                    to="Mumbai";
                    break;

                    case 2:
                    trainno=1014; 
                    trainname="Dehradun Delhi Super Express";
                    timeofboarding="7.00am    "+bookdate+" August 2012";                                    
                    timeofreaching="12.00pm    "+(bookdate)+" August 2012";
                    System.out.println("Train Number: "+trainno);
                    System.out.println("Train Name: "+trainname);
                    System.out.println("Time of Boarding: "+timeofboarding);
                    System.out.println("Time of Reaching: "+timeofreaching);
                    from="Dehradun";
                    to="Delhi";
                    break;

                    case 3:
                    trainno=1015; 
                    trainname="Dehradun Kolkata express";
                    if(bookdate==30)
                    {
                        timeofreaching="1.30pm    "+(date1)+" September 2012";
                    }
                    else
                    {
                        timeofreaching="1.30pm    "+(date1)+" August 2012";
                    }
                    timeofboarding="7.30am    "+bookdate+" August 2012";                                    

                    System.out.println("Train Number: "+trainno);
                    System.out.println("Train Name: "+trainname);
                    System.out.println("Time of Boarding: "+timeofboarding);
                    System.out.println("Time of Reaching: "+timeofreaching); 
                    from="Dehradun";
                    to="Kolkata";
                    break;

                    case 5:
                    trainno=1016; 
                    trainname="Dehradun Chennai non-stop Express";
                    if(bookdate==30)
                    {
                        timeofreaching="7.30am    "+(date2)+" September 2012";
                    }
                    else
                    {
                        timeofreaching="7.30am    "+(date2)+" August 2012";
                    }
                    timeofboarding="8.30am    "+bookdate+" August 2012";                                    

                    System.out.println("Train Number: "+trainno);
                    System.out.println("Train Name: "+trainname);
                    System.out.println("Time of Boarding: "+timeofboarding);
                    System.out.println("Time of Reaching: "+timeofreaching); 
                    from="Dehradun";
                    to="Chennai";
                    break;

                }
                break;

                //train list if station of board is 5
                case 5:
                switch(destination)
                {
                    case 1:
                    trainno=1017; 
                    trainname="Chennai Rail";
                    timeofboarding="5.00am    "+bookdate+" August 2012";                                    
                    timeofreaching="3.30pm    "+(date2)+" August 2012";
                    System.out.println("Train Number: "+trainno);
                    System.out.println("Train Name: "+trainname);
                    System.out.println("Time of Boarding: "+timeofboarding);
                    System.out.println("Time of Reaching: "+timeofreaching); 
                    from="Chennai";
                    to="Mumbai";
                    break;

                    case 2:
                    trainno=1018; 
                    trainname="Chennai Fast Train";
                    if(bookdate==30)
                    {
                        timeofreaching="8.00pm    "+(date2)+" September 2012";
                    }
                    else
                    {
                        timeofreaching="8.00pm    "+(date2)+" August 2012";
                    }
                    timeofboarding="8.30am    "+bookdate+" August 2012";                                    

                    System.out.println("Train Number: "+trainno);
                    System.out.println("Train Name: "+trainname);
                    System.out.println("Time of Boarding: "+timeofboarding);
                    System.out.println("Time of Reaching: "+timeofreaching);
                    from="Chennai";
                    to="Delhi";
                    break;

                    case 3:
                    trainno=1019; 
                    trainname="Chennai Kolkata express";
                    if(bookdate==30)
                    {
                        timeofreaching="6.30pm    "+(date1)+" September 2012";
                    }
                    else
                    {
                        timeofreaching="6.30pm    "+(date1)+" August 2012";
                    }
                    timeofboarding="6.30am    "+bookdate+" August 2012";                                    

                    System.out.println("Train Number: "+trainno);
                    System.out.println("Train Name: "+trainname);
                    System.out.println("Time of Boarding: "+timeofboarding);
                    System.out.println("Time of Reaching: "+timeofreaching); 
                    from="Chennai";
                    to="Kolkata";
                    break;

                    case 4:
                    trainno=1020; 
                    trainname="Chennai Dehradun Fast Rail";
                    if(bookdate==30)
                    {
                        timeofreaching="3.00am    "+(date2)+" September 2012";
                    }
                    else
                    {
                        timeofreaching="3.00am    "+(date2)+" August 2012";
                    }
                    timeofboarding="5.00am    "+bookdate+" August 2012";                                    

                    System.out.println("Train Number: "+trainno);
                    System.out.println("Train Name: "+trainname);
                    System.out.println("Time of Boarding: "+timeofboarding);
                    System.out.println("Time of Reaching: "+timeofreaching); 
                    from="Chennai";
                    to="Dehradun";
                    break; 
                }
                break;
            }
            //asking user whether to continue or not
            b=0;
            while (b==0)
            {
                System.out.println("Do you want to continue? y/n");
                ch = (char) new InputStreamReader(System.in).read ();
                if(ch=='Y'||ch=='y')
                {
                    b=1;
                }
                else if(ch=='n'||ch=='N')
                {
                    b=1;
                    break label;
                }

                else
                {
                    b=0;

                    while (b==0)
                    {
                        System.out.println("No such option, Do you want to try again? y/n");
                        ch = (char) new InputStreamReader(System.in).read ();
                        if(ch=='Y'||ch=='y')
                        {
                            b=1;
                        }
                        else if(ch=='n'||ch=='N')
                        {
                            b=1;
                            break label;
                        }

                        else
                        {}
                    }
                    b=0;
                }
            }
            // selecting coach
            a=0;
            while(a==0)
            {

                System.out.println("Please Select from 1-4 as class");
                System.out.println("1. General/Sleeper");
                System.out.println("2. AC 3");
                System.out.println("3. AC 2");
                System.out.println("4. AC 1");
                coach=Integer.parseInt(br.readLine());
                if(coach>4||coach<1)
                {
                    b=0;
                    while (b==0)
                    {
                        System.out.println("Wrong choice, Do you want to try again? y/n");
                        ch = (char) new InputStreamReader(System.in).read ();
                        if(ch=='Y'||ch=='y')
                        {
                            b=1;
                        }
                        else if(ch=='n'||ch=='N')
                        {
                            b=1;
                            break label;
                        }

                        else
                        {}
                    }
                }
                else
                {
                    a=1;
                }
            }
            //Vacancy Chart
            switch(coach)
            {
                case 1:
                //checks if file of a caoch of train on the given date exists or not
                File f = new File(bookdate+"a"+trainno+"a"+coach+".txt");
                if(f.exists())
                {
                    //if file exists it reads from the file and prints vacancy table
                    FileInputStream fstream = new FileInputStream(bookdate+"a"+trainno+"a"+coach+".txt");
                    DataInputStream in = new DataInputStream(fstream);
                    BufferedReader br1 = new BufferedReader(new InputStreamReader(in));
                    String strLine;
                    for (grow = 0; grow <= 3; grow++)
                    {
                        for (gcol = 0; gcol <=17; gcol++)
                        {
                            strLine = br1.readLine();
                            GenCompart[grow][gcol] = Integer.parseInt(strLine); 
                        }
                    }
                    for (grow = 0; grow <= 3; grow++)
                    {
                        for (gcol = 0; gcol <= 17; gcol++)
                        {
                            if (GenCompart[grow][gcol] < 100)
                            {
                                System.out.print(GenCompart[grow][gcol]);
                            }
                            else
                            {
                                System.out.print("X");
                            }
                            if(GenCompart[grow][gcol] < 10)
                            {
                                System.out.print("   ");
                            }
                            else
                            {
                                System.out.print("  ");
                            }
                            if(GenCompart[grow][gcol]%2==0)
                            {
                                System.out.print("|  ");
                            }
                        }

                        System.out.println(" ");
                        if (grow == 2) 
                        {
                            System.out.println(" ");
                        }
                    }
                }
                else
                {
                    //if file does not exist, iw writes the file and then reads from it and shows vacancy table
                    BufferedWriter bw = new BufferedWriter(new FileWriter(bookdate+"a"+trainno+"a"+coach+".txt"));
                    for(i=1;i<73;i++)
                    {
                        j= new Integer(i).toString();
                        bw.write(j);
                        bw.newLine();
                    }
                    bw.flush();
                    bw.close();

                    FileInputStream fstream = new FileInputStream(bookdate+"a"+trainno+"a"+coach+".txt");
                    DataInputStream in = new DataInputStream(fstream);
                    BufferedReader br1 = new BufferedReader(new InputStreamReader(in));
                    String strLine;
                    for (grow = 0; grow <= 3; grow++)
                    {
                        for (gcol = 0; gcol <=17; gcol++)
                        {
                            strLine = br1.readLine();
                            GenCompart[grow][gcol] = Integer.parseInt(strLine); 
                        }
                    }
                    for (grow = 0; grow <= 3; grow++)
                    {
                        for (gcol = 0; gcol <= 17; gcol++)
                        {
                            if (GenCompart[grow][gcol] < 100)
                            {
                                System.out.print(GenCompart[grow][gcol]);
                            }
                            else
                            {
                                System.out.print("X");
                            }
                            if(GenCompart[grow][gcol] < 10)
                            {
                                System.out.print("   ");
                            }
                            else
                            {
                                System.out.print("  ");
                            }
                            if(GenCompart[grow][gcol]%2==0)
                            {
                                System.out.print("|  ");
                            }
                        }

                        System.out.println(" ");
                        if (grow == 2) 
                        {
                            System.out.println(" ");
                        }
                    }
                }
                //takes a random number as the resrvation number
                randomnum = 100+(int)(Math.random()*1000);
                System.out.println("Seats Showing X are already booked");
                System.out.println("");
                a=1;
                while (a==1)
                {
                    //selecting a seat
                    System.out.println("Please choose a seat(Give the seat number)");
                    seatno=Integer.parseInt(br.readLine());

                    for (grow = 0; grow <= 3; grow++)
                    {
                        for (gcol = 0; gcol <=17; gcol++)
                        {

                            if (seatno==GenCompart[grow][gcol])
                            {
                                //replacing seat number by reservation number
                                a=0;
                                reservationno=randomnum;
                                GenCompart[grow][gcol]= reservationno;

                            }

                        }
                    }
                    //writing the vacancy table in a txt file
                    BufferedWriter bw = new BufferedWriter(new FileWriter(bookdate+"a"+trainno+"a"+coach+".txt"));
                    for (grow = 0; grow <= 3; grow++)
                    {
                        for (gcol = 0; gcol <=17; gcol++)
                        {

                            j= new Integer(GenCompart[grow][gcol]).toString();
                            bw.write(j);
                            bw.newLine();

                            bw.flush();

                        }
                    }
                    bw.close();

                    while (a==1)
                    {
                        //asking if seat is not available, asking to choose andother seat
                        System.out.println("Seat not available, Do you want to try again? y/n");
                        ch = (char) new InputStreamReader(System.in).read ();
                        if(ch=='Y'||ch=='y')
                        {
                            a=0;
                        }
                        else if(ch=='n'||ch=='N')
                        {
                            a=0;
                            break label;
                        }

                        else
                        {
                            b=0;
                            while (b==0)
                            {
                                System.out.println("Wrong Choice, Do you want to try again? y/n");
                                ch = (char) new InputStreamReader(System.in).read ();
                                if(ch=='Y'||ch=='y')
                                {
                                    b=1;
                                }
                                else if(ch=='n'||ch=='N')
                                {
                                    b=1;
                                    break label;
                                }

                                else
                                {}
                            }
                        }
                    }
                    //asking if another seat is required
                    System.out.println("Do you want to select another seat?y/n");
                    ch = (char) new InputStreamReader(System.in).read ();
                    k=k+1;
                    if(ch=='Y'||ch=='y')
                    {
                        a=1;
                    }
                    else if(ch=='n'||ch=='N')
                    {
                        a=0;
                    }
                    else
                    {
                        b=0;
                        while (b==0)
                        {
                            System.out.println("Wrong Choice, Do you want to try again? y/n");
                            ch = (char) new InputStreamReader(System.in).read ();
                            if(ch=='Y'||ch=='y')
                            {
                                b=1;
                                a=1;
                            }
                            else if(ch=='n'||ch=='N')
                            {
                                b=1;
                                break label;
                            }

                            else
                            {}
                        }
                    }
                }

                //deisplaying the fare
                fare=1000.00;
                totalfare=fare*k;
                System.out.println("");
                System.out.println("Single Fare="+fare);
                System.out.println("Total Fare="+totalfare);
                //taking the user's deatils
                System.out.println("Please enter your name");
                name= br.readLine();
                System.out.println("Please enter your address");
                address=br.readLine();
                System.out.println("Please enter your age");
                age=Integer.parseInt(br.readLine());
                System.out.println("Please enter your gender(m/f)");
                gender=(char) new InputStreamReader(System.in).read ();

                System.out.println("Your ticket will be printed in a word file.Please copy it to the desktop.");

                //writing the ticket
                BufferedWriter bw = new BufferedWriter(new FileWriter(bookdate+"a"+trainno+"a"+coach+"ticket.doc"));
                bw.write("Name:"+name);
                bw.newLine();
                bw.write("Address:"+address);
                bw.newLine();
                bw.write("Age:"+age);
                bw.newLine();
                bw.write("Gender:"+name);
                bw.newLine();
                bw.write("Train Name:"+trainname);
                bw.newLine();
                bw.write("Train Number:"+trainno);
                bw.newLine();
                bw.write("Station of Boarding:"+from);
                bw.newLine();
                bw.write("Destination:"+to);
                bw.newLine();
                bw.write("Coach:General");
                bw.newLine();
                bw.write("Time of Boarding:"+timeofboarding);
                bw.newLine();
                bw.write("Time of Reaching:"+timeofreaching);
                bw.newLine();
                bw.write("Reservation Number:"+reservationno);
                bw.newLine();
                bw.write("Fare per Passenger: Rs."+fare);
                bw.newLine();
                bw.write("Total Fare: Rs."+totalfare);
                bw.newLine();
                bw.flush();
                bw.close();

                break;

                case 2:
                //does the same as it did for general compartment
                f = new File(bookdate+"a"+trainno+"a"+coach+".txt");
                if(f.exists())
                {
                    FileInputStream fstream = new FileInputStream(bookdate+"a"+trainno+"a"+coach+".txt");
                    DataInputStream in = new DataInputStream(fstream);
                    BufferedReader br1 = new BufferedReader(new InputStreamReader(in));
                    String strLine;
                    for (grow = 0; grow <= 3; grow++)
                    {
                        for (gcol = 0; gcol <=17; gcol++)
                        {
                            strLine = br1.readLine();
                            GenCompart[grow][gcol] = Integer.parseInt(strLine); 
                        }
                    }
                    for (grow = 0; grow <= 3; grow++)
                    {
                        for (gcol = 0; gcol <= 17; gcol++)
                        {
                            if (GenCompart[grow][gcol] < 100)
                            {
                                System.out.print(GenCompart[grow][gcol]);
                            }
                            else
                            {
                                System.out.print("X");
                            }
                            if(GenCompart[grow][gcol] < 10)
                            {
                                System.out.print("   ");
                            }
                            else
                            {
                                System.out.print("  ");
                            }
                            if(GenCompart[grow][gcol]%2==0)
                            {
                                System.out.print("|  ");
                            }
                        }

                        System.out.println(" ");
                        if (grow == 2) 
                        {
                            System.out.println(" ");
                        }
                    }
                }
                else
                {
                    BufferedWriter bw1 = new BufferedWriter(new FileWriter(bookdate+"a"+trainno+"a"+coach+".txt"));
                    for(i=1;i<73;i++)
                    {
                        j= new Integer(i).toString();
                        bw1.write(j);
                        bw1.newLine();
                    }
                    bw1.flush();
                    bw1.close();

                    FileInputStream fstream = new FileInputStream(bookdate+"a"+trainno+"a"+coach+".txt");
                    DataInputStream in = new DataInputStream(fstream);
                    BufferedReader br1 = new BufferedReader(new InputStreamReader(in));
                    String strLine;
                    for (grow = 0; grow <= 3; grow++)
                    {
                        for (gcol = 0; gcol <=17; gcol++)
                        {
                            strLine = br1.readLine();
                            GenCompart[grow][gcol] = Integer.parseInt(strLine); 
                        }
                    }
                    for (grow = 0; grow <= 3; grow++)
                    {
                        for (gcol = 0; gcol <= 17; gcol++)
                        {
                            if (GenCompart[grow][gcol] < 100)
                            {
                                System.out.print(GenCompart[grow][gcol]);
                            }
                            else
                            {
                                System.out.print("X");
                            }
                            if(GenCompart[grow][gcol] < 10)
                            {
                                System.out.print("   ");
                            }
                            else
                            {
                                System.out.print("  ");
                            }
                            if(GenCompart[grow][gcol]%2==0)
                            {
                                System.out.print("|  ");
                            }
                        }

                        System.out.println(" ");
                        if (grow == 2) 
                        {
                            System.out.println(" ");
                        }
                    }
                }
                randomnum = 100+(int)(Math.random()*1000);
                System.out.println("Seats Showing X are already booked");
                System.out.println("");
                a=1;
                while (a==1)
                {

                    System.out.println("Please choose a seat(Give the seat number)");
                    seatno=Integer.parseInt(br.readLine());

                    for (grow = 0; grow <= 3; grow++)
                    {
                        for (gcol = 0; gcol <=17; gcol++)
                        {

                            if (seatno==GenCompart[grow][gcol])
                            {
                                a=0;
                                reservationno=randomnum;
                                GenCompart[grow][gcol]= reservationno;

                            }

                        }
                    }
                    BufferedWriter bw1 = new BufferedWriter(new FileWriter(bookdate+"a"+trainno+"a"+coach+".txt"));
                    for (grow = 0; grow <= 3; grow++)
                    {
                        for (gcol = 0; gcol <=17; gcol++)
                        {

                            j= new Integer(GenCompart[grow][gcol]).toString();
                            bw1.write(j);
                            bw1.newLine();

                            bw1.flush();

                        }
                    }
                    bw1.close();

                    while (a==1)
                    {
                        System.out.println("Seat not available, Do you want to try again? y/n");
                        ch = (char) new InputStreamReader(System.in).read ();
                        if(ch=='Y'||ch=='y')
                        {
                            a=0;
                        }
                        else if(ch=='n'||ch=='N')
                        {
                            a=0;
                            break label;
                        }

                        else
                        {
                            b=0;
                            while (b==0)
                            {
                                System.out.println("Wrong Choice, Do you want to try again? y/n");
                                ch = (char) new InputStreamReader(System.in).read ();
                                if(ch=='Y'||ch=='y')
                                {
                                    b=1;
                                }
                                else if(ch=='n'||ch=='N')
                                {
                                    b=1;
                                    break label;
                                }

                                else
                                {}
                            }
                        }
                    }
                    System.out.println("Do you want to select another seat?y/n");
                    ch = (char) new InputStreamReader(System.in).read ();
                    k=k+1;
                    if(ch=='Y'||ch=='y')
                    {
                        a=1;
                    }
                    else if(ch=='n'||ch=='N')
                    {
                        a=0;
                    }
                    else
                    {
                        b=0;
                        while (b==0)
                        {
                            System.out.println("Wrong Choice, Do you want to try again? y/n");
                            ch = (char) new InputStreamReader(System.in).read ();
                            if(ch=='Y'||ch=='y')
                            {
                                b=1;
                                a=1;
                            }
                            else if(ch=='n'||ch=='N')
                            {
                                b=1;
                                break label;
                            }

                            else
                            {}
                        }
                    }
                }

                fare=2000.00;
                totalfare=fare*k;
                System.out.println("");
                System.out.println("Single Fare="+fare);
                System.out.println("Total Fare="+totalfare);
                System.out.println("Please enter your name");
                name= br.readLine();
                System.out.println("Please enter your address");
                address=br.readLine();
                System.out.println("Please enter your age");
                age=Integer.parseInt(br.readLine());
                System.out.println("Please enter your gender(m/f)");
                gender=(char) new InputStreamReader(System.in).read ();

                System.out.println("Your ticket will be printed in a word file");

                BufferedWriter bw1 = new BufferedWriter(new FileWriter(bookdate+"a"+trainno+"a"+coach+"ticket.doc"));
                bw1.write("Name:"+name);
                bw1.newLine();
                bw1.write("Address:"+address);
                bw1.newLine();
                bw1.write("Age:"+age);
                bw1.newLine();
                bw1.write("Gender:"+name);
                bw1.newLine();
                bw1.write("Train Name:"+trainname);
                bw1.newLine();
                bw1.write("Train Number:"+trainno);
                bw1.newLine();
                bw1.write("Station of Boarding:"+from);
                bw1.newLine();
                bw1.write("Destination:"+to);
                bw1.newLine();
                bw1.write("Coach:AC3");
                bw1.newLine();
                bw1.write("Time of Boarding:"+timeofboarding);
                bw1.newLine();
                bw1.write("Time of Reaching:"+timeofreaching);
                bw1.newLine();
                bw1.write("Reservation Number:"+reservationno);
                bw1.newLine();
                bw1.write("Fare per Passenger: Rs."+fare);
                bw1.newLine();
                bw1.write("Total Fare: Rs."+totalfare);
                bw1.newLine();
                bw1.flush();
                bw1.close();

                break;

                case 3:
                //does the same as it did for  general
                f = new File(bookdate+"a"+trainno+"a"+coach+".txt");
                if(f.exists())
                {
                    FileInputStream fstream = new FileInputStream(bookdate+"a"+trainno+"a"+coach+".txt");
                    DataInputStream in = new DataInputStream(fstream);
                    BufferedReader br1 = new BufferedReader(new InputStreamReader(in));
                    String strLine;
                    for (grow = 0; grow <= 2; grow++)
                    {
                        for (gcol = 0; gcol <=17; gcol++)
                        {
                            strLine = br1.readLine();
                            GenCompart[grow][gcol] = Integer.parseInt(strLine); 
                        }
                    }
                    for (grow = 0; grow <= 2; grow++)
                    {
                        for (gcol = 0; gcol <= 17; gcol++)
                        {
                            if (GenCompart[grow][gcol] < 100)
                            {
                                System.out.print(GenCompart[grow][gcol]);
                            }
                            else
                            {
                                System.out.print("X");
                            }
                            if(GenCompart[grow][gcol] < 10)
                            {
                                System.out.print("   ");
                            }
                            else
                            {
                                System.out.print("  ");
                            }
                            if(GenCompart[grow][gcol]%2==0)
                            {
                                System.out.print("|  ");
                            }
                        }

                        System.out.println(" ");
                        if (grow == 1) 
                        {
                            System.out.println(" ");
                        }
                    }
                }
                else
                {
                    BufferedWriter bw2 = new BufferedWriter(new FileWriter(bookdate+"a"+trainno+"a"+coach+".txt"));
                    for(i=1;i<55;i++)
                    {
                        j= new Integer(i).toString();
                        bw2.write(j);
                        bw2.newLine();
                    }
                    bw2.flush();
                    bw2.close();

                    FileInputStream fstream = new FileInputStream(bookdate+"a"+trainno+"a"+coach+".txt");
                    DataInputStream in = new DataInputStream(fstream);
                    BufferedReader br1 = new BufferedReader(new InputStreamReader(in));
                    String strLine;
                    for (grow = 0; grow <= 2; grow++)
                    {
                        for (gcol = 0; gcol <=17; gcol++)
                        {
                            strLine = br1.readLine();
                            GenCompart[grow][gcol] = Integer.parseInt(strLine); 
                        }
                    }
                    for (grow = 0; grow <= 2; grow++)
                    {
                        for (gcol = 0; gcol <= 17; gcol++)
                        {
                            if (GenCompart[grow][gcol] < 100)
                            {
                                System.out.print(GenCompart[grow][gcol]);
                            }
                            else
                            {
                                System.out.print("X");
                            }
                            if(GenCompart[grow][gcol] < 10)
                            {
                                System.out.print("   ");
                            }
                            else
                            {
                                System.out.print("  ");
                            }
                            if(GenCompart[grow][gcol]%2==0)
                            {
                                System.out.print("|  ");
                            }
                        }

                        System.out.println(" ");
                        if (grow == 2) 
                        {
                            System.out.println(" ");
                        }
                    }
                }
                randomnum = 100+(int)(Math.random()*1000);
                System.out.println("Seats Showing X are already booked");
                System.out.println("");
                a=1;
                while (a==1)
                {

                    System.out.println("Please choose a seat(Give the seat number)");
                    seatno=Integer.parseInt(br.readLine());

                    for (grow = 0; grow <= 2; grow++)
                    {
                        for (gcol = 0; gcol <=17; gcol++)
                        {

                            if (seatno==GenCompart[grow][gcol])
                            {
                                a=0;
                                reservationno=randomnum;
                                GenCompart[grow][gcol]= reservationno;

                            }

                        }
                    }
                    BufferedWriter bw2 = new BufferedWriter(new FileWriter(bookdate+"a"+trainno+"a"+coach+".txt"));
                    for (grow = 0; grow <= 2; grow++)
                    {
                        for (gcol = 0; gcol <=17; gcol++)
                        {

                            j= new Integer(GenCompart[grow][gcol]).toString();
                            bw2.write(j);
                            bw2.newLine();

                            bw2.flush();

                        }
                    }
                    bw2.close();

                    while (a==1)
                    {
                        System.out.println("Seat not available, Do you want to try again? y/n");
                        ch = (char) new InputStreamReader(System.in).read ();
                        if(ch=='Y'||ch=='y')
                        {
                            a=0;
                        }
                        else if(ch=='n'||ch=='N')
                        {
                            a=0;
                            break label;
                        }

                        else
                        {
                            b=0;
                            while (b==0)
                            {
                                System.out.println("Wrong Choice, Do you want to try again? y/n");
                                ch = (char) new InputStreamReader(System.in).read ();
                                if(ch=='Y'||ch=='y')
                                {
                                    b=1;
                                }
                                else if(ch=='n'||ch=='N')
                                {
                                    b=1;
                                    break label;
                                }

                                else
                                {}
                            }
                        }
                    }
                    System.out.println("Do you want to select another seat?y/n");
                    ch = (char) new InputStreamReader(System.in).read ();
                    k=k+1;
                    if(ch=='Y'||ch=='y')
                    {
                        a=1;
                    }
                    else if(ch=='n'||ch=='N')
                    {
                        a=0;
                    }
                    else
                    {
                        b=0;
                        while (b==0)
                        {
                            System.out.println("Wrong Choice, Do you want to try again? y/n");
                            ch = (char) new InputStreamReader(System.in).read ();
                            if(ch=='Y'||ch=='y')
                            {
                                b=1;
                                a=1;
                            }
                            else if(ch=='n'||ch=='N')
                            {
                                b=1;
                                break label;
                            }

                            else
                            {}
                        }
                    }
                }

                fare=3000.00;
                totalfare=fare*k;
                System.out.println("");
                System.out.println("Single Fare="+fare);
                System.out.println("Total Fare="+totalfare);
                System.out.println("Please enter your name");
                name= br.readLine();
                System.out.println("Please enter your address");
                address=br.readLine();
                System.out.println("Please enter your age");
                age=Integer.parseInt(br.readLine());
                System.out.println("Please enter your gender(m/f)");
                gender=(char) new InputStreamReader(System.in).read ();

                System.out.println("Your ticket will be printed in a word file");

                BufferedWriter bw2 = new BufferedWriter(new FileWriter(bookdate+"a"+trainno+"a"+coach+"ticket.doc"));
                bw2.write("Name:"+name);
                bw2.newLine();
                bw2.write("Address:"+address);
                bw2.newLine();
                bw2.write("Age:"+age);
                bw2.newLine();
                bw2.write("Gender:"+name);
                bw2.newLine();
                bw2.write("Train Name:"+trainname);
                bw2.newLine();
                bw2.write("Train Number:"+trainno);
                bw2.newLine();
                bw2.write("Station of Boarding:"+from);
                bw2.newLine();
                bw2.write("Destination:"+to);
                bw2.newLine();
                bw2.write("Coach:AC2");
                bw2.newLine();
                bw2.write("Time of Boarding:"+timeofboarding);
                bw2.newLine();
                bw2.write("Time of Reaching:"+timeofreaching);
                bw2.newLine();
                bw2.write("Reservation Number:"+reservationno);
                bw2.newLine();
                bw2.write("Fare per Passenger: Rs."+fare);
                bw2.newLine();
                bw2.write("Total Fare: Rs."+totalfare);
                bw2.newLine();
                bw2.flush();
                bw2.close();

                break;

                case 4:
                //does the same as it did for general
                f = new File(bookdate+"a"+trainno+"a"+coach+".txt");
                if(f.exists())
                {
                    FileInputStream fstream = new FileInputStream(bookdate+"a"+trainno+"a"+coach+".txt");
                    DataInputStream in = new DataInputStream(fstream);
                    BufferedReader br1 = new BufferedReader(new InputStreamReader(in));
                    String strLine;
                    for (grow = 0; grow <= 1; grow++)
                    {
                        for (gcol = 0; gcol <=14; gcol++)
                        {
                            strLine = br1.readLine();
                            GenCompart[grow][gcol] = Integer.parseInt(strLine); 
                        }
                    }
                    for (grow = 0; grow <= 1; grow++)
                    {
                        for (gcol = 0; gcol <= 14; gcol++)
                        {
                            if (GenCompart[grow][gcol] < 100)
                            {
                                System.out.print(GenCompart[grow][gcol]);
                            }
                            else
                            {
                                System.out.print("X");
                            }
                            if(GenCompart[grow][gcol] < 10)
                            {
                                System.out.print("   ");
                            }
                            else
                            {
                                System.out.print("  ");
                            }
                            if(GenCompart[grow][gcol]%2==0)
                            {
                                System.out.print("|  ");
                            }
                        }

                        System.out.println(" ");

                    }
                }
                else
                {
                    BufferedWriter bw3 = new BufferedWriter(new FileWriter(bookdate+"a"+trainno+"a"+coach+".txt"));
                    for(i=1;i<29;i++)
                    {
                        j= new Integer(i).toString();
                        bw3.write(j);
                        bw3.newLine();
                    }
                    bw3.flush();
                    bw3.close();

                    FileInputStream fstream = new FileInputStream(bookdate+"a"+trainno+"a"+coach+".txt");
                    DataInputStream in = new DataInputStream(fstream);
                    BufferedReader br1 = new BufferedReader(new InputStreamReader(in));
                    String strLine;
                    for (grow = 0; grow <= 1; grow++)
                    {
                        for (gcol = 0; gcol <=13; gcol++)
                        {
                            strLine = br1.readLine();
                            GenCompart[grow][gcol] = Integer.parseInt(strLine); 
                        }
                    }
                    for (grow = 0; grow <= 1; grow++)
                    {
                        for (gcol = 0; gcol <= 14; gcol++)
                        {
                            if (GenCompart[grow][gcol] < 100)
                            {
                                System.out.print(GenCompart[grow][gcol]);
                            }
                            else
                            {
                                System.out.print("X");
                            }
                            if(GenCompart[grow][gcol] < 10)
                            {
                                System.out.print("   ");
                            }
                            else
                            {
                                System.out.print("  ");
                            }
                            if(GenCompart[grow][gcol]%2==0)
                            {
                                System.out.print("|  ");
                            }
                        }

                        System.out.println(" ");

                    }
                }
                randomnum = 100+(int)(Math.random()*1000);
                System.out.println("Seats Showing X are already booked");
                System.out.println("");
                a=1;
                while (a==1)
                {

                    System.out.println("Please choose a seat(Give the seat number)");
                    seatno=Integer.parseInt(br.readLine());

                    for (grow = 0; grow <= 1; grow++)
                    {
                        for (gcol = 0; gcol <=14; gcol++)
                        {

                            if (seatno==GenCompart[grow][gcol])
                            {
                                a=0;
                                reservationno=randomnum;
                                GenCompart[grow][gcol]= reservationno;

                            }

                        }
                    }
                    BufferedWriter bw3 = new BufferedWriter(new FileWriter(bookdate+"a"+trainno+"a"+coach+".txt"));
                    for (grow = 0; grow <= 1; grow++)
                    {
                        for (gcol = 0; gcol <=14; gcol++)
                        {

                            j= new Integer(GenCompart[grow][gcol]).toString();
                            bw3.write(j);
                            bw3.newLine();

                            bw3.flush();

                        }
                    }
                    bw3.close();

                    while (a==1)
                    {
                        System.out.println("Seat not available, Do you want to try again? y/n");
                        ch = (char) new InputStreamReader(System.in).read ();
                        if(ch=='Y'||ch=='y')
                        {
                            a=0;
                        }
                        else if(ch=='n'||ch=='N')
                        {
                            a=0;
                            break label;
                        }

                        else
                        {
                            b=0;
                            while (b==0)
                            {
                                System.out.println("Wrong Choice, Do you want to try again? y/n");
                                ch = (char) new InputStreamReader(System.in).read ();
                                if(ch=='Y'||ch=='y')
                                {
                                    b=1;
                                }
                                else if(ch=='n'||ch=='N')
                                {
                                    b=1;
                                    break label;
                                }

                                else
                                {}
                            }
                        }
                    }
                    System.out.println("Do you want to select another seat?y/n");
                    ch = (char) new InputStreamReader(System.in).read ();
                    k=k+1;
                    if(ch=='Y'||ch=='y')
                    {
                        a=1;
                    }
                    else if(ch=='n'||ch=='N')
                    {
                        a=0;
                    }
                    else
                    {
                        b=0;
                        while (b==0)
                        {
                            System.out.println("Wrong Choice, Do you want to try again? y/n");
                            ch = (char) new InputStreamReader(System.in).read ();
                            if(ch=='Y'||ch=='y')
                            {
                                b=1;
                                a=1;
                            }
                            else if(ch=='n'||ch=='N')
                            {
                                b=1;
                                break label;
                            }

                            else
                            {}
                        }
                    }
                }

                fare=4000.00;
                totalfare=fare*k;
                System.out.println("");
                System.out.println("Single Fare="+fare);
                System.out.println("Total Fare="+totalfare);
                System.out.println("Please enter your name");
                name= br.readLine();
                System.out.println("Please enter your address");
                address=br.readLine();
                System.out.println("Please enter your age");
                age=Integer.parseInt(br.readLine());
                System.out.println("Please enter your gender(m/f)");
                gender=(char) new InputStreamReader(System.in).read ();

                System.out.println("Your ticket will be printed in a word file");

                BufferedWriter bw3 = new BufferedWriter(new FileWriter(bookdate+"a"+trainno+"a"+coach+"ticket.doc"));
                bw3.write("Name:"+name);
                bw3.newLine();
                bw3.write("Address:"+address);
                bw3.newLine();
                bw3.write("Age:"+age);
                bw3.newLine();
                bw3.write("Gender:"+name);
                bw3.newLine();
                bw3.write("Train Name:"+trainname);
                bw3.newLine();
                bw3.write("Train Number:"+trainno);
                bw3.newLine();
                bw3.write("Station of Boarding:"+from);
                bw3.newLine();
                bw3.write("Destination:"+to);
                bw3.newLine();
                bw3.write("Coach:AC1");
                bw3.newLine();
                bw3.write("Time of Boarding:"+timeofboarding);
                bw3.newLine();
                bw3.write("Time of Reaching:"+timeofreaching);
                bw3.newLine();
                bw3.write("Reservation Number:"+reservationno);
                bw3.newLine();
                bw3.write("Fare per Passenger: Rs."+fare);
                bw3.newLine();
                bw3.write("Total Fare: Rs."+totalfare);
                bw3.newLine();
                bw3.flush();
                bw3.close();

                break;

            }
        }

        else
        {
            z=0;
            while(z==0)
            {
                //for cancellation
                //taking details
                System.out.println("Please enter your reservation number");
                reservationno=Integer.parseInt(br.readLine());
                System.out.println("Please enter your travelling date");
                bookdate=Integer.parseInt(br.readLine());
                System.out.println("Please enter your trainno");
                trainno=Integer.parseInt(br.readLine());
                a=0;
                while(a==0)
                {

                    System.out.println("Please select your class(1-4)");
                    System.out.println("1. General/Sleeper");
                    System.out.println("2. AC 3");
                    System.out.println("3. AC 2");
                    System.out.println("4. AC 1");
                    coach=Integer.parseInt(br.readLine());
                    if(coach>4||coach<1)
                    {
                        b=0;
                        while (b==0)
                        {
                            System.out.println("Wrong choice, Do you want to try again? y/n");
                            ch = (char) new InputStreamReader(System.in).read ();
                            if(ch=='Y'||ch=='y')
                            {
                                b=1;
                            }
                            else if(ch=='n'||ch=='N')
                            {
                                b=1;
                                break label;
                            }

                            else
                            {}
                        }
                    }
                    else
                    {
                        a=1;
                    }
                }
                //if ticket exitss cancels it by replacing reservation  number by seat number,else says that ticket does not exist
                File f = new File(bookdate+"a"+trainno+"a"+coach+".txt");
                if(f.exists())
                {
                    z=1;
                }
                else
                {
                    while (b==0)
                    {
                        System.out.println("No such Ticket exists, Do you want to try again? y/n");
                        ch = (char) new InputStreamReader(System.in).read ();
                        if(ch=='Y'||ch=='y')
                        {
                            b=1;
                        }
                        else if(ch=='n'||ch=='N')
                        {
                            b=1;
                            break label;
                        }

                        else
                        {}
                    }
                }
            }
            switch(coach)
            {
                case 1:

                FileInputStream fstream = new FileInputStream(bookdate+"a"+trainno+"a"+coach+".txt");
                DataInputStream in = new DataInputStream(fstream);
                BufferedReader br1 = new BufferedReader(new InputStreamReader(in));
                String strLine;
                for (i = 1; i <= 72; i++)
                {

                    strLine = br1.readLine();
                    GenCompart1[i] = Integer.parseInt(strLine); 
                    if (GenCompart1[i]==reservationno)
                    {
                        //replacing by seat number
                        GenCompart1[i]=i;
                    }

                }
                BufferedWriter bw = new BufferedWriter(new FileWriter(bookdate+"a"+trainno+"a"+coach+".txt"));
                for (i = 1; i <= 72; i++)
                {

                    j= new Integer(GenCompart1[i]).toString();
                    bw.write(j);
                    bw.newLine();

                    bw.flush();

                }
                bw.close();
                System.out.println("Your Ticket has been cancelled");

                break;

                case 2:
                FileInputStream fstream1 = new FileInputStream(bookdate+"a"+trainno+"a"+coach+".txt");
                DataInputStream in1 = new DataInputStream(fstream1);
                BufferedReader br2 = new BufferedReader(new InputStreamReader(in1));
                strLine="0";
                for (i = 1; i <= 72; i++)
                {

                    strLine = br2.readLine();
                    GenCompart1[i] = Integer.parseInt(strLine); 
                    if (GenCompart1[i]==reservationno)
                    {
                        GenCompart1[i]=i;
                    }

                }
                BufferedWriter bw4 = new BufferedWriter(new FileWriter(bookdate+"a"+trainno+"a"+coach+".txt"));
                for (i = 1; i <= 72; i++)
                {

                    j= new Integer(GenCompart1[i]).toString();
                    bw4.write(j);
                    bw4.newLine();

                    bw4.flush();

                }
                bw4.close();
                System.out.println("Your Ticket has been cancelled");

                break;

                case 3:
                FileInputStream fstream2 = new FileInputStream(bookdate+"a"+trainno+"a"+coach+".txt");
                DataInputStream in2 = new DataInputStream(fstream2);
                BufferedReader br3 = new BufferedReader(new InputStreamReader(in2));

                for (i = 1; i <= 54; i++)
                {

                    strLine = br3.readLine();
                    GenCompart1[i] = Integer.parseInt(strLine); 
                    if (GenCompart1[i]==reservationno)
                    {
                        GenCompart1[i]=i;
                    }

                }
                BufferedWriter bw5 = new BufferedWriter(new FileWriter(bookdate+"a"+trainno+"a"+coach+".txt"));
                for (i = 1; i <= 54; i++)
                {

                    j= new Integer(GenCompart1[i]).toString();
                    bw5.write(j);
                    bw5.newLine();

                    bw5.flush();

                }
                bw5.close();
                System.out.println("Your Ticket has been cancelled");

                break;

                case 4:
                FileInputStream fstream5 = new FileInputStream(bookdate+"a"+trainno+"a"+coach+".txt");
                DataInputStream in5 = new DataInputStream(fstream5);
                BufferedReader br5 = new BufferedReader(new InputStreamReader(in5));

                for (i = 1; i <= 28; i++)
                {

                    strLine = br5.readLine();
                    GenCompart1[i] = Integer.parseInt(strLine); 
                    if (GenCompart1[i]==reservationno)
                    {
                        GenCompart1[i]=i;
                    }

                }
                BufferedWriter bw6 = new BufferedWriter(new FileWriter(bookdate+"a"+trainno+"a"+coach+".txt"));
                for (i = 1; i <= 28; i++)
                {

                    j= new Integer(GenCompart1[i]).toString();
                    bw6.write(j);
                    bw6.newLine();

                    bw6.flush();

                }
                bw6.close();
                System.out.println("Your Ticket has been cancelled");

                break;

            }
        }
    }
}

